# ---- Base Node ---- #
FROM ubuntu:16.04 AS base
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y build-essential libtool autotools-dev autoconf libevent-dev libssl-dev git-core libboost-all-dev libdb4.8-dev libdb4.8++-dev libminiupnpc-dev pkg-config sudo && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# ---- Build Source ---- #
FROM base as build
RUN git clone https://github.com/vertcoin/vertcoin.git /opt/vertcoin
RUN cd /opt/vertcoin/ && \
    ./autogen.sh && \
    ./configure --without-miniupnpc --disable-tests --enable-debug --with-gui=no
RUN cd /opt/vertcoin/ && \
    make -j2

# ---- Release ---- #
FROM ubuntu:16.04 as release
RUN apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 8842ce5e && \
    echo "deb http://ppa.launchpad.net/bitcoin/bitcoin/ubuntu xenial main" > /etc/apt/sources.list.d/bitcoin.list
RUN apt-get update && \
    apt-get install -y libboost-all-dev libdb4.8 libdb4.8++ libminiupnpc-dev libevent-dev && \
    apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
RUN groupadd -r vertcoin && useradd -r -m -g vertcoin vertcoin
RUN mkdir /data
RUN chown vertcoin:vertcoin /data
COPY --from=build /opt/vertcoin/src/vertcoind /usr/local/bin/vertcoind
COPY --from=build /opt/vertcoin/src/vertcoin-cli /usr/local/bin/vertcoin-cli
USER vertcoin
VOLUME /data
EXPOSE 5888 5889
CMD ["/usr/local/bin/vertcoind", "-datadir=/data", "-conf=/data/vertcoin.conf", "-server", "-txindex", "-printtoconsole"]